//include SDL2 and stdio libs
#include <SDL2/SDL.h>
#include <stdio.h>

//window size
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

int main( int argc, char* args[] ){
    //makes the window to use
    SDL_Window* window = NULL;

    //makes the surface of the window
    SDL_Surface* screenSurface = NULL;

    //Start SDL
    if( SDL_Init( SDL_INIT_VIDEO ) < 0 ){
        printf("Ooops SDL could not start! SDL_Error: %s\n", SDL_GetError() );
    } else {
        //make window
        window = SDL_CreateWindow( "My First SDL2 Window", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == NULL ){
            printf("Oh no the Window could not be made! SDL_Error: %s\n", SDL_GetError() );        
        } else {
                //Get window surface
                screenSurface = SDL_GetWindowSurface( window );

                //Display white in the window
                SDL_FillRect( screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0xff, 0xff, 0xff ) );

                //Update the surface
                SDL_UpdateWindowSurface( window );

                //wait five seconds
                SDL_Delay( 5000 );
            }
    }
    
    //Close Window
    SDL_DestroyWindow( window );

    //End SDL
    SDL_Quit();

    return 0;
}


